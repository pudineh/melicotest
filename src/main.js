import Vue from 'vue'
import App from './App.vue'
import store from './store'
import VuePersianDatetimePicker from 'vue-persian-datetime-picker';


Vue.component('date-picker', VuePersianDatetimePicker);
Vue.use(VuePersianDatetimePicker, {
  name: 'date-picker',
  props: {
      color: '#fd9a35'
  }
});
Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
