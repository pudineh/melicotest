let tableData = {
  "data": [
    {
      "title": "فروش محصول اول",
      "total": 12400,
      "count": 227
    },
    {
      "title": "فروش محصول دوم",
      "total": 132000,
      "count": 15
    },
    {
      "title": "بازگشتی ها",
      "total": 32000,
      "count": 2
    }
  ]
}
let statistics = {
  "data": [
    {
      "name": "محمد علی",
      "family_name": "عابدینی گلستانی",
      "age": 27,
      "balance": 1200000,
      "user_code": "user-12424",
      "user_description": "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.",
      "actions": [
        "remove",
        "edit"
      ]
    },
    {
      "name": "داوود",
      "family_name": "کیانی",
      "age": 18,
      "balance": 1300000,
      "user_code": "user-12425",
      "user_description": "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.",
      "actions": [
        "remove"
      ]
    },
    {
      "name": "سارا",
      "family_name": "جمشیدی",
      "age": 22,
      "balance": 1450000,
      "user_code": "user-12426",
      "user_description": "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.",
      "actions": [
        "edit"
      ]
    },
    {
      "name": "دانیال",
      "family_name": "خسروجردی",
      "age": 26,
      "balance": 900000,
      "user_code": "user-12427",
      "user_description": "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.",
      "actions": [
        "remove",
        "edit"
      ]
    },
    {
      "name": "اکبر",
      "family_name": "رحمانی",
      "age": 25,
      "balance": 1300000,
      "user_code": "user-12428",
      "user_description": "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.",
      "actions": [
        "remove"
      ]
    },
    {
      "name": "مریم",
      "family_name": "رادکام",
      "age": 28,
      "balance": 2450000,
      "user_code": "user-12429",
      "user_description": "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.",
      "actions": []
    },
    {
      "name": "بهرام",
      "family_name": "محمدی",
      "age": 28,
      "balance": 1350000,
      "user_code": "user-12430",
      "user_description": "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.",
      "actions": [
        "edit"
      ]
    },
    {
      "name": "حامد",
      "family_name": "نبوی کیانی",
      "age": 16,
      "balance": 2500000,
      "user_code": "user-12431",
      "user_description": "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.",
      "actions": [
        "remove",
        "edit"
      ]
    },
    {
      "name": "کاوه",
      "family_name": "اکبری",
      "age": 21,
      "balance": 100000,
      "user_code": "user-12432",
      "user_description": "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.",
      "actions": []
    },
    {
      "name": "شادی",
      "family_name": "کیکاووسی",
      "age": 19,
      "balance": 1220000,
      "user_code": "user-12433",
      "user_description": "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.",
      "actions": []
    }
  ]
}
export {tableData, statistics}